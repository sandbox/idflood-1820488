<?php

/**
 * @file
 * Rules definitions for scheduler module
 */

/**
 * Implements hook_rules_event_info().
 */
function scheduler_rules_rules_event_info() {
  $defaults = array(
    'group' => t('scheduler'),
    'module' => 'scheduler',
    'access callback' => 'scheduler_rules_rules_access',
  );
  return array(
    'node_scheduler_publish' => $defaults + array(
      'label' => t('After a scheduled node is published'),
      'variables' => array(
        'node' => array('type' => 'node', 'label' => t('published node')),
      ),
    ),
    'node_scheduler_unpublish' => $defaults + array(
      'label' => t('After a scheduled node is unpublished'),
      'variables' => array(
        'node' => array('type' => 'node', 'label' => t('unpublished node')),
      ),
    ),
  );
}

/**
 * Access callback for scheduler rules.
 */
function scheduler_rules_rules_access($type, $name) {
  if ($type == 'event') {
    return TRUE;
  }
}
